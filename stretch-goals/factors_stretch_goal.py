def factors(number):
    # ==============
    # Your code here
    result = []

    for i in range(1, number + 1):
        if number % i == 0:
            result.append(i)

    result = result[1:-1]

    if result == []:
        a = str(number)
        b = '"' + a
        c = ' is a prime number"'
        result = b + c
    else:
         result = result
         
    return result
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
