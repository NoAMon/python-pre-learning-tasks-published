def vowel_swapper(string):
    # ==============
    # Your code here
    wip = string

    wip0 = wip.lower()

    #a
    wip1 = wip0.find('a', wip0.find('a')+1)
    if wip1 > -1:
        wip1a = list(wip)
        wip1a[wip1] = '4'
        wip1b = "".join(wip1a)
    else:
        wip1b = wip

    #e
    wip2 = wip0.find('e', wip0.find('e')+1)
    if wip2 > -1:
        wip2a = list(wip1b)
        wip2a[wip2] = '3'
        wip2b = "".join(wip2a)
    else:
        wip2b = wip1b

    #i
    wip3 = wip0.find('i', wip0.find('i')+1)
    if wip3 > -1:
        wip3a = list(wip2b)
        wip3a[wip3] = '!'
        wip3b = "".join(wip3a)
    else:
        wip3b = wip2b

    #o
    wip4 = wip0.find('o', wip0.find('o')+1)
    if wip4 > -1:
        wip4a = list(wip3b)
        if wip4a[wip4] == 'o':
            wip4a[wip4] = 'ooo'
        else:
            wip4a[wip4] = '000'
        wip4b = "".join(wip4a)
    else:
        wip4b = wip3b

    wipofix = wip4b.lower()

    #u
    wip5 = wipofix.find('u', wipofix.find('u')+1)
    if wip5 > -1:
        wip5a = list(wip4b)
        wip5a[wip5] = '|_|'
        wip5b = "".join(wip5a)
    else:
        wip5b = wip4b

    result = wip5b
    return result
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
